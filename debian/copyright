Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ktimer
Source: ftp://ftp.kde.org

Files: *
Copyright: 2001, KDE Team
           2001-2002, Meni Livne <livne@kde.org>
           2001, Stefan Schimanski <schimmi@kde.org>
License: GPL-2+

Files: doc/*
       po/ca/docs/*
       po/cs/docs/*
       po/de/docs/*
       po/es/docs/*
       po/et/docs/*
       po/it/docs/*
       po/nl/docs/*
       po/pt/docs/*
       po/pt_BR/docs/*
       po/sr/docs/*
       po/sv/docs/*
       po/uk/*
Copyright: 2008, A.L. Spehr
           2001, Stefan Schimanski
License: GFDL-1.2+

Files: po/uk/ktimer.po
Copyright: 2002-2018, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

Files: debian/*
Copyright: 2012, Eshat Cakar <info@eshat.de>
License: GPL-2+

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License version 1.2 can be found in "/usr/share/common-licenses/GFDL-1.2".

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+3+KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

